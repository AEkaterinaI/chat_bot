import time
import requests
import json

TOKEN = '5889820812:AAHu_qASmeEjZEYKRH6ArqERVQWNjOGQzXc'
URL = 'https://api.telegram.org/bot'

#получение новых сообщений
def get_updates(offset=0):
    result = requests.get(f'{URL}{TOKEN}/getUpdates?offset={offset}').json()
    return result['result']

# Текстовое сообщение
def send_message(chat_id, text):
    requests.get(f'{URL}{TOKEN}/sendMessage?chat_id={chat_id}&text={text}')

def send_photo_url(chat_id, img_url):
    requests.get(f'{URL}{TOKEN}/sendPhoto?chat_id={chat_id}&photo={img_url}')

# Текст сообщения
def send_text_message(chat_id, text):
    params = {
        "text": text,
        "parse_mode" : "MarkdownV2"
    }
    requests.post(f'{URL}{TOKEN}/sendMessage?chat_id={chat_id}', params=params)

# Отправка фото на сервер
def send_photo_file(chat_id, img):
    files = {'photo': open(img, 'rb')}
    requests.post(f'{URL}{TOKEN}/sendPhoto?chat_id={chat_id}', files=files)

def send_photo_file_id(chat_id, file_id):
    requests.get(f'{URL}{TOKEN}/sendPhoto?chat_id={chat_id}&photo={file_id}')

# Голосовое сообщение
def send_voice_file(chat_id, voice):
    files = {'voice' : open (voice, 'rb' )}
    requests.post(f'{URL}{TOKEN}/sendVoice?chat_id={chat_id}', files=files)

# Аудио-сообщение
def send_audio_file(chat_id):
    url = f'https://api.telegram.org/bot{TOKEN}/sendAudio'
    payload = {
        'chat_id' : chat_id ,
        "audio" : "http://www.largesound.com/ashborytour/sound/brobob.mp3" ,

    }
    requests.post ( url , json=payload )

# Документ
def send_docs_file(chat_id, doc):
    with open(doc, "rb") as file:
        files = {"document":file}
        requests.post(f'{URL}{TOKEN}/sendDocument?chat_id={chat_id}', files=files)

# Видео
def send_video_file(chat_id):
    url = f'https://api.telegram.org/bot{TOKEN}/sendVideo'
    payload = {
        'chat_id' : chat_id ,
        "video" : "https://www.appsloveworld.com/wp-content/uploads/2018/10/640.mp4" ,

    }
    requests.post (url, json=payload )

# Медиагруппа
def send_mediagroup_file(chat_id):
    request_url = "https://api.telegram.org/bot" + TOKEN + "/sendMediaGroup"
    files = {
        "random-name-1" : open ( "photo.jpg", "rb" )  #ссылка на локальный файл
        , "random-name-2" : open ( "photo.jpg", "rb" )  #ссылка на локальный файл
    }
    params = {
        "chat_id" : chat_id
        , "media" :
            """[
                {
                    "type": "photo"
                    , "media": "attach://random-name-1"
                    ,"caption": "текст для отправки"},
                {
                    "type": "photo"
                    , "media": "attach://random-name-2"}
            ]"""
    }

    requests.post(request_url, params= params, files= files)

# Контакты
def send_contact(chat_id,number, firstname):
    params = {
        "phone_number" : number,
        "first_name" : firstname
    }
    requests.post(f'{URL}{TOKEN}/sendContact?chat_id={chat_id}', params=params)

# местоположение
def send_location(chat_id,latitude, longitude ):
    params = {
        "latitude" : latitude,
        "longitude": longitude
    }
    requests.post(f'{URL}{TOKEN}/sendLocation?chat_id={chat_id}', params=params)

# Опрос
def send_poll(chat_id):
    url = f'{URL}{TOKEN}/sendPoll'
    payload = {
        'chat_id' : chat_id ,
        "question" : "In which direction does the sun rise?" ,
        "options" : json.dumps ( [ "North" , "South" , "East" , "West" ] ) ,
        "is_anonymous" : False ,
        "type" : "quiz" ,
        "correct_option_id" : 2
    }
    requests.post ( url , json=payload )

# Варианты действий
def reply_keyboard(chat_id, text):
    reply_markup ={ "keyboard": [["Фото с компьютера", "Голосовое сообщение", "Текстовое сообщение", "Аудио-сообщение", "Документ"],
    ["Видео","Медиагруппа","Контакт", "Местоположение","Опрос"]], "resize_keyboard": True, "one_time_keyboard": True}
    data = {'chat_id': chat_id, 'text': text, 'reply_markup': json.dumps(reply_markup)}
    requests.post(f'{URL}{TOKEN}/sendMessage', data=data)

# Ответы на сообщения
def check_message(chat_id, message):
    if message.lower() in ['привет', 'hello']:
        send_message(chat_id, 'Привет')
    elif message.lower() in 'фото по url':
        # Отправить URL-адрес картинки (телеграм скачает его и отправит)
        send_photo_url(chat_id, 'https://ramziv.com/static/assets/img/home-bg.jpg')
    elif message.lower() in 'текстовое сообщение':
        text = "Привет это текст"
        send_text_message(chat_id, text)
    elif message.lower() in 'фото с компьютера':
        # Отправить файл с компьютера
        send_photo_file(chat_id, 'photo.jpg')
    elif message.lower() in 'фото с сервера телеграм':
        # Отправить id файла (файл уже хранится где-то на серверах Telegram)
        send_photo_file_id(chat_id, 'AgACAgIAAxkBAAMqYVGBbdbivL53IzKLfUKUClBnB0cAApy0MRtfMZBKHL0tNw9aITwBAAMCAAN4AAMhBA')
    elif message.lower() in 'голосовое сообщение':
        send_voice_file(chat_id,'voice.mp3')
    elif message.lower() in 'аудио-сообщение':
        send_audio_file(chat_id)
    elif message.lower() in 'документ':
        send_docs_file(chat_id,'doc.pdf')
    elif message.lower() in 'видео':
        send_video_file(chat_id)
    elif message.lower() in 'медиагруппа':
        send_mediagroup_file(chat_id)
    elif message.lower() in 'контакт':
        send_contact(chat_id, "+79773588076", "Алишер")
    elif message.lower () in 'местоположение' :
        latitude = 56.842157
        longitude = 60.649092
        send_location ( chat_id , latitude , longitude )
    elif message.lower () in 'опрос' :
        send_poll ( chat_id )
    else:
        reply_keyboard(chat_id, 'Вот что я умею')

def run():
    update_id = get_updates()[-1]['update_id'] # Сохраняем ID последнего отправленного сообщения боту
    while True:
        time.sleep(2)
        messages = get_updates(update_id) # Получаем обновления
        for message in messages:
            # Если в обновлении есть ID больше чем ID последнего сообщения, значит пришло новое сообщение
            if update_id < message['update_id']:
                update_id = message['update_id']# Сохраняем ID последнего отправленного сообщения боту
                if (user_message := message['message'].get('text')): # Проверим, есть ли текст в сообщении
                    check_message(message['message']['chat']['id'], user_message) # Отвечаем

if __name__ == '__main__':
    run()